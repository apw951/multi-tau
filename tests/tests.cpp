#include <stdint.h>
#include <iostream>
#include <random>
#include <fstream>
#include <chrono>

#include <langevinSimulator.h>
#include <correlator.h>

const double tol = 1e-6;

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

SCENARIO("Langevin Motion", "[]"){
  GIVEN("A Langevin Particle Trajectory of length N=10000"){
    
    const unsigned N = 10000;
    float trajectory[2*N];

    AND_GIVEN("Diffusion Coefficient 0.1, and time step 1.0/60.0, unit mass and unit friction"){

      double D = 0.1;
      float dt = 1.0/60.0;

      WHEN("Simulating the particle with seed 314159"){

        LangevinParticle<double> p(D,314159);
        LangevinSimulator<double> sim;

        AND_GIVEN("The Naive Correlator"){

          auto start = std::chrono::high_resolution_clock::now();
          Naive<double> correlator(
            Correland::Velocity,
            Correland::Velocity
          );

          sim.simulate(
            p,
            1.0/60.0,
            N,
            &correlator,
            1001
          );
          auto end = std::chrono::high_resolution_clock::now();
          std::chrono::duration<double> diff = end - start;
          std::cout << "Naive took " << diff.count() << " s";

          THEN("The velocity autocorrelation falls of exponentially"){
            // not really a well formed test, least squares?
            auto c = sim.getCorrelations(1000);
            //REQUIRE(std::abs(c[0][0])<0.1);

            std::ofstream out("naive.txt");
            if (out.is_open()){
              for (unsigned i = 0; i < 1000; i++){
                auto cor = sim.getCorrelations(i);
                for (unsigned n = 0; n < cor.size(); n++){
                  for (unsigned m = 0; m < cor[n].size(); m++){
                    out << cor[n][m] << ", ";
                  }
                }
                out << "\n";
              }
            }
            out.close();

            sim.save("velocity.txt");
          }
        }
        AND_GIVEN("The MultiTau Correlator"){

          auto start = std::chrono::high_resolution_clock::now();
          MultiTau<double> correlator(
            32,1000,2,
            Correland::Velocity,
            Correland::Velocity
          );

          sim.simulate(
            p,
            1.0/60.0,
            N,
            &correlator,
            1001
          );
          auto end = std::chrono::high_resolution_clock::now();
          std::chrono::duration<double> diff = end - start;
          std::cout << "MultiTau took " << diff.count() << " s";

          THEN("The velocity autocorrelation falls of exponentially"){
            // not really a well formed test, least squares?
            auto c = sim.getCorrelations(1000);
            //REQUIRE(std::abs(c[0][0])<0.1);

            std::ofstream out("multiTau.txt");
            if (out.is_open()){
              auto cor = correlator.getCorrelation();
              for (unsigned i = 0; i < cor.first.size(); i++){
                for (unsigned n = 0; n < cor.second[0].size(); n++){
                  for (unsigned m = 0; m < cor.second[0][n].size(); m++){
                    out << cor.second[i][n][m] << ", ";
                  }
                }
                out << "\n";
              }
            }
            out.close();
          }
        }
      }
    }
  }
}