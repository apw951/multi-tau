#!/bin/bash
RELEASE=0
TEST=0
while [[ $# -gt 0 ]]; do
  case $1 in
    -r|--release)
      RELEASE=1
      shift
      ;;
    -t|--test)
      TEST=1
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done


for file in build CMakeFiles cmake_install.cmake CMakeCache.txt Makefile
do
  if [ -d $file ];
  then
    rm -rf $file
  fi
  if [ -f $file ];
  then
    rm $file
  fi
done

mkdir build && cd build
cmake -D RELEASE=$RELEASE -D TEST_SUITE=$TEST .. && make
cd ../