#include <langevinSimulator.h>
#include <correlator.h>
#include <fstream>
#include <sstream>

int main()
{

    const unsigned N = 10000;
    const double D = 0.1;
    const float dt = 1.0/60.0;

    const std::vector<unsigned> blocks = {1,2,3,4,5,6,7,8};
    const std::vector<unsigned> points = {128,256,512,1024};
    const std::vector<unsigned> windows = {2,4,8,16,32};

    LangevinParticle<double> p(D,314159);
    LangevinSimulator<double> sim;

    Naive<double> correlator(
        Correland::Velocity,
        Correland::Velocity
    );

    sim.simulate(
        p,
        dt,
        N,
        &correlator,
        blocks.back()*points.back()
    );

    std::ofstream out("reference");
    float tau = 0.0;
    for (unsigned t = 0; t < blocks.back()*points.back(); t++)
    {
        auto c = sim.getCorrelations(t);
        out << tau << ", " << c[1][1] << "\n";
        tau += dt;
    }
    out.close();

    unsigned k = 1;
    for (unsigned b = 0; b < blocks.size(); b++)
    {
        for (unsigned p = 0; p < points.size(); p++)
        {
            for (unsigned w = 0; w < windows.size(); w++)
            {
                std::cout << "\r " 
                          << k 
                          << "/" 
                          << blocks.size()*points.size()*windows.size() 
                          << " | [" 
                          << blocks[b] 
                          << ", "
                          << points[p]
                          << ", "
                          << windows[w]
                          << "]"
                          << std::flush;

                LangevinParticle<double> particle(D,314159);
                LangevinSimulator<double> sim;

                MultiTau<double> correlator(
                    blocks[b], points[p], windows[w],
                    Correland::Velocity,
                    Correland::Velocity
                );

                sim.simulate(
                    particle,
                    dt,
                    N,
                    &correlator,
                    blocks[b]*points[p]+1
                );

                std::stringstream name;
                name << "mtau-" << blocks[b] << "-" << points[p] << "-" << windows[w];
                std::ofstream out(name.str());
                float tau = 0.0;
                auto cor = correlator.getCorrelation();
                for (unsigned t = 0; t < cor.first.size(); t++)
                {   
                    out << cor.first[t]*dt << ", " << cor.second[t][1][1] << "\n";
                }
                out.close();
                k++;
            }
        }
    }

    
}

