#ifndef LANGEVINSIMULATOR
#define LANGEVINSIMULATOR

#include <vector>
#include <iostream>
#include <random>
#include <limits>
#include <typeinfo>
#include <fstream>

#include <langevinParticle.h>
#include <correlator.h>

template <class T>
class LangevinSimulator {

public:

    LangevinSimulator() = default;

    void simulate(
        LangevinParticle<T> & p, 
        T timeStep,
        unsigned steps,
        Correlator<T> * c,
        unsigned maxCorrelationTime
    ){

        trajectory.clear();
        correlations.clear();

        c->init(2,2);

        T D = std::sqrt(2.0*p.diffusion/timeStep);
        eng.seed(p.seed);
        trajectory = std::vector<T>(steps*2,0.0);

        for (unsigned i = 1; i < steps; i++){
            T a = N(eng)*D-trajectory[(i-1)*2+1];
            T v = trajectory[(i-1)*2+1]+timeStep*a;
            T r = trajectory[(i-1)*2]+trajectory[(i-1)*2+1]*timeStep;

            trajectory[i*2] = r;
            trajectory[i*2+1] = v;

            c->update(
                {r,v},
                {r,v}
            );
        }

        // calculate the velocity auto-correlation (needs abstraction)

        std::vector<std::vector<std::vector<T>>> cor = c->getCorrelation().second;

        for (unsigned i = 0; i < cor.size(); i++){
            correlations.push_back(
                cor[i]
            );
        }
    }

    std::vector<std::vector<T>> getCorrelations(unsigned tau){
        return correlations[tau];
    }

    void save(std::string file){
        std::ofstream out(file);
        std::cout << trajectory.size() << "\n";
        if (out.is_open()){
            for (unsigned i = 0; i < trajectory.size(); i++){
                if (i%2==1){out << trajectory[i] << "\n";}
            }
            out.close();
        }
    }

private:

    std::vector<T> trajectory;
    std::vector<std::vector<std::vector<T>>> correlations;

    std::normal_distribution<T> N;
    std::default_random_engine eng;


};

#endif /* LANGEVINSIMULATOR */
