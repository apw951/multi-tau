#ifndef LANGEVINPARTICLE
#define LANGEVINPARTICLE

template <class T>
struct LangevinParticle {

    LangevinParticle(
        T d,
        unsigned s
    )
    : diffusion(d), seed(s)
    {}

    T diffusion;
    unsigned seed;
};

#endif /* LANGEVINPARTICLE */
