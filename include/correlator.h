#ifndef CORRELATOR
#define CORRELATOR

#include <limits>
#include <algorithm>
#include <exception>
#include <string>
#include <utility>

#include <naiveCorrelator.h>

class DimensionError : public std::exception {

public:

    DimensionError(std::string msg)
    : msg(msg)
    {}

    const char * what () {
            return msg.c_str();
        }

private:

    std::string msg;

};

enum class Correland {
    Position,
    Velocity,
    Force,
    PositonMagnitude,
    VelocityMagnitude,
    ForceMagnitude
};

/*

    Base correlator type with an accumulator, and template (T double or float)

*/
template <class T>
struct Correlator{
    
    Correlator(
        Correland l,
        Correland r
    )
    : left(l), right(r)
    {}
    virtual ~Correlator(){}
    virtual void init(unsigned n, unsigned m) = 0;
    virtual void update(std::vector<T> left, std::vector<T> right) = 0;
    virtual std::pair<std::vector<T>, std::vector<std::vector<std::vector<T>>>> getCorrelation() = 0;

    void update(T left, T right){
        update(
            std::vector<T>{left},
            std::vector<T>{right}
        );
    }

    std::vector<std::vector<T>> accumulatorLeft;
    std::vector<std::vector<T>> accumulatorRight;

    Correland left;
    Correland right;

    unsigned leftSize;
    unsigned rightSize;

};

/*

    Accumulates all velocities
    Explicity calculates the correlation function  after a simulation has ended

*/
template <class T>
struct Naive : public Correlator<T> {
    Naive(
        Correland l,
        Correland r
    )
    : Correlator<T>(l,r)
    {}

    void init(unsigned n, unsigned m){
        this->accumulatorLeft.clear();
        this->accumulatorRight.clear();
        this->leftSize = n;
        this->rightSize = m;
    }

    void update(std::vector<T> left, std::vector<T> right){

        if (left.size() != this->leftSize || right.size() != this->rightSize){
            throw DimensionError(
                "Got dimensions " + std::to_string(left.size()) + " and " + std::to_string(right.size()) + " expected " + std::to_string(this->leftSize) + " and " + std::to_string(this->rightSize) + " for correlation function "
            );
        }

        this->accumulatorLeft.push_back(left);
        this->accumulatorRight.push_back(right);
    }
    
    std::pair<std::vector<T>, std::vector<std::vector<std::vector<T>>>> getCorrelation(){
        std::vector<std::vector<std::vector<T>>> cor;
        std::vector<T> times;
        for (unsigned tau = 0; tau < this->accumulatorLeft.size(); tau++){
            cor.push_back(
                naiveCorrelator<T>(
                    this->accumulatorLeft,
                    this->accumulatorRight,
                    this->accumulatorLeft.size(),
                    tau
                )
            );
            times.push_back(tau);
        }
        return std::pair<std::vector<T>, std::vector<std::vector<std::vector<T>>>>(times, cor);
    }
};

template <class T>
std::vector<T> operator/(std::vector<T> v, T s){

    T c = 1.0/s;

    std::vector<T> R(v.size());
    
    std::transform(v.begin(), v.end(), R.begin(), [c](T element) {
        return element * c;
    });

    return R;
}

template <class T>
struct MultiTau : public Correlator<T> {
    MultiTau(
        unsigned n, 
        unsigned p, 
        unsigned w,
        Correland l,
        Correland r
    )
    : Correlator<T>(l,r), 
    numberOfCorrelators(n), 
    pointsPerCorrelator(p), 
    windowSize(w)
    {
        minimumDistance = pointsPerCorrelator/windowSize;
    }

    void init(unsigned n, unsigned m){

        this->leftSize = n; this->rightSize = m;

        shiftLeft.clear();
        shiftRight.clear();
        countsCorrelation.clear();
        correlation.clear();

        for (unsigned i = 0; i < numberOfCorrelators; i++){
            shiftLeft.push_back(
                std::vector<std::vector<T>>(pointsPerCorrelator,std::vector<T>(n,0.0))
            );
            shiftRight.push_back(
                std::vector<std::vector<T>>(pointsPerCorrelator,std::vector<T>(m,0.0))
            );
            countsCorrelation.push_back(
                std::vector<unsigned>(pointsPerCorrelator,0)
            );
            correlation.push_back(
                std::vector<std::vector<std::vector<T>>>(pointsPerCorrelator,std::vector<std::vector<T>>(n,std::vector<T>(m,0.0)))
            );
        }

        for (unsigned i = 0; i < numberOfCorrelators; i++){
            for (unsigned j = 0; j < pointsPerCorrelator; j++){
                for (unsigned ni = 0; ni < this->leftSize; ni++){
                    shiftLeft[i][j][ni] = NULL_SHIFT;
                }
                for (unsigned mj = 0; mj < this->rightSize; mj++){
                    shiftRight[i][j][mj] = NULL_SHIFT;
                }
                for (unsigned ni = 0; ni < this->leftSize; ni++){
                    for (unsigned mj = 0; mj < this->rightSize; mj++){
                        correlation[i][j][ni][mj] = 0.0;
                    }
                }
                countsCorrelation[i][j] = 0;
            }
            this->accumulatorLeft.push_back(std::vector<T>(n,0.0));
            this->accumulatorRight.push_back(std::vector<T>(m,0.0));

            countsAccumulator.push_back(0);
            currentIndex.push_back(0);
        }

        accumulationResultLeft = std::vector<T>(n,0.0);
        accumulationResultRight = std::vector<T>(m,0.0);
        maxCorrelatorUsed = 0;
    }

    void update(std::vector<T> left, std::vector<T> right){

        if (left.size() != this->leftSize || right.size() != this->rightSize){
            throw DimensionError(
                "Got dimensions " + std::to_string(left.size()) + " and " + std::to_string(right.size()) + " expected " + std::to_string(this->leftSize) + " and " + std::to_string(this->rightSize) + " for correlation function "
            );
        }

        add(left, right, 0);
    }

    void add(std::vector<T> left, std::vector<T> right, unsigned correlator){
        // reached the limit
        if (correlator == numberOfCorrelators){return;}
        // indicate this correlator has data
        if (correlator > maxCorrelatorUsed){maxCorrelatorUsed=correlator;}

        // insert new value into register
        for (unsigned n = 0; n < this->leftSize; n++){
            shiftLeft[correlator][currentIndex[correlator]][n] = left[n];
        }
        for (unsigned m = 0; m < this->rightSize; m++){
            shiftRight[correlator][currentIndex[correlator]][m] = right[m];
        }
        // add to main average
        if (correlator == 0){
            for (unsigned n = 0; n < this->leftSize; n++){
                accumulationResultLeft[n] += left[n];
            }
            for (unsigned m = 0; m < this->rightSize; m++){
                accumulationResultRight[m] += right[m];
            }
        }

        // accumulate
        for (unsigned n = 0; n < this->leftSize; n++){
            this->accumulatorLeft[correlator][n] += left[n];
        }

        for (unsigned m = 0; m < this->rightSize; m++){
            this->accumulatorRight[correlator][m] += right[m];
        }

        countsAccumulator[correlator] += 1;
        
        if (countsAccumulator[correlator]==windowSize){
            // filled this window, pass average recursively to
            // next correlator
            add(
                this->accumulatorLeft[correlator]/static_cast<T>(windowSize),
                this->accumulatorRight[correlator]/static_cast<T>(windowSize),
                correlator+1
            );
            // can reset correlator at this level
            this->accumulatorLeft[correlator] = std::vector<T>(this->leftSize,0.0);
            this->accumulatorRight[correlator] = std::vector<T>(this->rightSize,0.0);
            countsAccumulator[correlator] = 0;
        }

        // now accumulate into the correlation
        unsigned i = currentIndex[correlator];
        if (correlator == 0){
            int j = i;
            for (unsigned n = 0; n<pointsPerCorrelator; n++){
                // if there is data, correlate it
                bool notNull = false;
                for (unsigned l = 0; l < this->leftSize; l++){
                    for (unsigned r = 0; r < this->rightSize; r++){
                        if ( shiftNotNull(shiftLeft[correlator][i][l]) && shiftNotNull(shiftRight[correlator][j][r]) ){
                            correlation[correlator][n][l][r] += shiftLeft[correlator][i][l]*shiftRight[correlator][j][r];
                            notNull = true;
                        }
                    }
                }
                if(notNull){countsCorrelation[correlator][n]++;}
                j--;
                if (j<0){j+=pointsPerCorrelator;}
            }
        }
        else{
            int j = i-minimumDistance;
            for (unsigned n = minimumDistance; n < pointsPerCorrelator; n++){
                if (j<0){j+=pointsPerCorrelator;}
                bool notNull = false;
                for (unsigned l = 0; l < this->leftSize; l++){
                    for (unsigned r = 0; r < this->rightSize; r++){
                        if ( shiftNotNull(shiftLeft[correlator][i][l]) && shiftNotNull(shiftRight[correlator][j][r]) ){
                            correlation[correlator][n][l][r] += shiftLeft[correlator][i][l]*shiftRight[correlator][j][r];
                            notNull = true;
                        }
                    }
                }
                if(notNull){countsCorrelation[correlator][n]++;}
                j--;
            }
        }

        currentIndex[correlator]++;
        if (currentIndex[correlator] == pointsPerCorrelator){
            currentIndex[correlator] = 0;
        }
    }

    std::pair<std::vector<T>, std::vector<std::vector<std::vector<T>>>>  getCorrelation(){
        std::vector<std::vector<std::vector<T>>> cor = std::vector<std::vector<std::vector<T>>>(
            pointsPerCorrelator*numberOfCorrelators,std::vector<std::vector<T>>(
                this->leftSize, std::vector<T>(
                    this->rightSize,0.0
                )
            )
        );

        std::vector<T> times;

        unsigned im = 0;
 
        for (unsigned i = 0; i < pointsPerCorrelator; i++){
            if (countsCorrelation[0][i] > 0){
                for (unsigned l = 0; l < this->leftSize; l++){
                    for (unsigned r = 0; r < this->rightSize; r++){
                        cor[im][l][r] += correlation[0][i][l][r]/countsCorrelation[0][i];
                    }
                }
                times.push_back(i);
                im++;
            }
        }


        for (unsigned k = 1; k < maxCorrelatorUsed; k++){
            for (unsigned i = minimumDistance; i < pointsPerCorrelator; i++){
                if (countsCorrelation[k][i] > 0){
                    for (unsigned l = 0; l < this->leftSize; l++){
                        for (unsigned r = 0; r < this->rightSize; r++){
                            cor[im][l][r] += correlation[k][i][l][r]/countsCorrelation[k][i];
                        }
                    }
                    im++;
                    times.push_back(i * std::pow(double(windowSize), k));
                }
            }

        }
        return std::pair<std::vector<T>, std::vector<std::vector<std::vector<T>>>>(times, cor);
    }

    unsigned numberOfCorrelators; 
    unsigned pointsPerCorrelator;
    unsigned windowSize;
    unsigned minimumDistance;

    unsigned maxCorrelatorUsed;

    std::vector<T> accumulationResultLeft;
    std::vector<T> accumulationResultRight;

    std::vector<std::vector<std::vector<T>>> shiftLeft;
    std::vector<std::vector<std::vector<T>>> shiftRight;

    std::vector<std::vector<std::vector<std::vector<T>>>> correlation;

    std::vector<std::vector<unsigned>> countsCorrelation;
    std::vector<unsigned> countsAccumulator;
    std::vector<unsigned> currentIndex;

    const T NULL_SHIFT = 1.0/0.0;

    bool shiftNotNull(T value){return value != NULL_SHIFT;}

};


#endif /* CORRELATOR */
