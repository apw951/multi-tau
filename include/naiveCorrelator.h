#ifndef NAIVECORRELATOR
#define NAIVECORRELATOR
#include <vector>
/*
    Naive correlator, requires complete data pass by samples pointer

    T:              a float or double
    timeSteps:      length of data in samples
    lagTimeSteps:   time lag to find correlation for
*/
template<class T>
std::vector<std::vector<T>> naiveCorrelator(
    std::vector<std::vector<T>> & samplesLeft,
    std::vector<std::vector<T>> & samplesRight,
    unsigned timeSteps,
    unsigned lagTimeSteps
){

    std::vector<std::vector<T>> sum = std::vector<std::vector<T>>(
        samplesLeft[0].size(),std::vector<T>(
            samplesRight[0].size(),0.0
        )
    );

    unsigned maxIndex = timeSteps-lagTimeSteps;
    for (unsigned i = 0; i < maxIndex; i++){
        for (unsigned n = 0; n < samplesLeft[0].size(); n++){
            for (unsigned m = 0; m < samplesRight[0].size(); m++){
                sum[n][m] += samplesLeft[i][n]*samplesRight[i+lagTimeSteps][m];
            }
        }
    }

    T norm = static_cast<T>(1.0)/static_cast<T>(maxIndex);
    for (unsigned n = 0; n < samplesLeft[0].size(); n++){
        for (unsigned m = 0; m < samplesRight[0].size(); m++){
            sum[n][m] *= norm;
        }
    }

    return sum;

}

#endif /* NAIVECORRELATOR */
